<?php 
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('1','cfg_name','网站标题','绿黑色物流网站设计G','2','基本设置','1')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('2','cfg_keywords','关键字','关键字','3','基本设置','2')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('3','cfg_description','描述','描述','3','基本设置','3')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('4','cfg_copyright','底部信息','Copyright@ 2013-2014 
版权所有：绿黑色物流网站设计G','3','基本设置','4')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('11','cfg_address','地址','软件园二期望海路2号','2','基本设置','6')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('9','cfg_image','图片上传格式','gif|png|jpg|jpeg','3','更多设置','2')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('6','cfg_logo','LOGO','./Data/Uploads/image/2015/01/14/54b67e83543e9.png','1','更多设置','1')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('7','cfg_icp','备案号','备案号','2','基本设置','5')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('8','cfg_count','引用','','3','更多设置','4')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('10','cfg_file','文件上传格式','doc|docx|ppt|pptx|xls|xlsx|zip|rar|7z|gif|png|jpg|jpeg|pdf','3','更多设置','3')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('12','cfg_tel','电话','0755-88888888','2','基本设置','7')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('13','cfg_email','邮箱','web@aaaa.com','2','基本设置','8')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('14','cfg_pic_small_width','图集小图宽','55','2','更多设置','6')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('15','cfg_pic_small_height','图集小图高','55','2','更多设置','7')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('16','cfg_pic_medium_width','图集中图宽','300','2','更多设置','8')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('17','cfg_pic_medium_height','图集中图高','300','2','更多设置','9')");
$db->execute("REPLACE INTO ".$db_prefix."config (`id`,`code`,`title`,`body`,`config_type`,`group`,`sort`)
						VALUES('18','cfg_map','百度地图地址','深圳市腾讯大厦','2','更多设置','10')");
