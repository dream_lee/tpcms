<?php
/**[权限表逻辑层模型]
 * @Author: chenli
 * @Email:  976123967@qq.com
 * @Date:   2015-02-23 23:15:41
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class AuthGroupAccessLogic extends Model{


	protected $tableName ='auth_group_access';


	public function alter_auth_group_access($uid)
	{
		$this->where(array('uid'=>$uid))->delete();
		$groupId = I('post.group_id');
		if(!$groupId)
			return ;
		foreach($groupId as $v)
		{
			$data[] =array(
				'group_id'=>$v,
				'uid'=>$uid,
			);
		}
		$this->addAll($data);
	}


	/**
	 * [delete_auth_group_access 删除]
	 * @return [type] [description]
	 */
	public function delete_auth_group_access()
	{
		$where['uid'] = I('get.uid');
		$this->where($where)->delete();
	}
}