<?php
/**[文档类型模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-28 10:19:03
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-26 09:55:14
 */
namespace Admin\Logic;
use Think\Model;
class TypeLogic extends Model{

	public function _initialize()
	{
		$this->cache = S('type');
	}
	protected $_validate = array(
		array('typename','require','请输入类型名称',1)
	);
	public function get_all()
	{
		return $this->cache;
	}
	public function update_cache()
	{
		$data = $this->order(array('typeid'=>'desc'))->select();
		$temp = array();
		if($data)
		{
			foreach($data as $k=>$v)
			{
				$temp[$v['typeid']] = $v;
			}
		}
		
		S('type',$temp);
	}

	public function get_one($typeid)
	{
		return isset($this->cache[$typeid])?$this->cache[$typeid]:null;
	}
	public function _after_insert($data,$options)
	{
		$this->update_cache();
	}
	public function _after_update($data,$options)
	{
		$this->update_cache();
	}
	public function del($typeid)
	{
		$status = D('attr')->where(array('type_typeid'=>$typeid))->find();
		if($status)
		{
			$this->error='请先删除属于类型属性';
			return false;
		}
		$this->delete($typeid);
	}

	
}
