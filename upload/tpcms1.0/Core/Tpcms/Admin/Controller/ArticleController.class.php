<?php
/**[文档控制器]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-14 15:30:10
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-06-08 14:24:59
 */
namespace Admin\Controller;
class ArticleController extends PublicController{
	


	
	/**
	 * [base 添加和编辑基本信息分配]
	 * @return [type] [description]
	 */
	private function base($cid)
	{
		$categoryLogic = D('Category');

		// 当前访问栏目所有父级栏目cid
		$parentCids  = $categoryLogic->get_parent_cid($cid);
		$parentcid   = $parentCids[count($parentCids)-1];
		$top         = $this->cache[$parentcid];
		$this->assign('topType' ,$top['cat_type']);
		$this->assign('topCname',$top['cname']);

		// 当前栏目顶级栏目的所有子分类
		$topChild    = $categoryLogic->get_top_child($cid);
		$this->assign('topChild',$topChild);
		// 文档属性
		$this->assign('flag',C('flag'));
	
	}
	/**
	 * [add 添加]
	 */
	public function add()
	{
		$categoryLogic = D('Category','Logic');
		if(IS_POST)
		{
			if(!$this->logic->create())	
				$this->error($this->logic->getError());
			$aid = $this->logic->add();
			$cid = I('post.category_cid');
			$verifystate = I('post.verifystate');
			$this->logic->add_ext($aid,$cid);
			$cur = $categoryLogic->get_one($cid);
			if($cur['cat_type']==4)
				$url = U('Index/copyright');
			else
				$url = U('index',array('category_cid'=>$cid,'verifystate'=>$verifystate));
			$this->success('添加成功',$url);	
		}
		else
		{
			$_SESSION['keditor'] = null;
			$cid = I('get.category_cid');
			$this->base($cid);
			// 当前访问的栏目信息
			$cur = $categoryLogic->get_one($cid);
			if($cur['cat_type']==4)
			{
				$data = $this->logic->get_one_by_cid($cid);
				if($data)
					$this->redirect('Article/edit',array('category_cid'=>$cid,'aid'=>$data['aid']));
			}

			$this->assign('curCatType',$cur['cat_type']);
			$this->assign('curCname',$cur['cname']);
			$this->assign('curCid',$cur['cid']);

			$pids = D('Category')->get_parent_cid($cid);
			$this->assign('pid',$pids[count($pids)-1]);
			
			//扩展表表单
			$extForm = D("ModelField","Logic")->get_field_form($cur['model_mid']);
			$this->assign('extForm',$extForm);


			// 读取栏目的文档类型表单
			$attrLogic = D('Attr',"Logic");
			$typeid = $cur['type_typeid'];
			$attrForm = $attrLogic->get_attr_form($typeid);
			$this->assign('attrForm',$attrForm);







			$this->display();
		}
		
	}

	/**
	 * [edit 编辑]
	 * @return [type] [description]
	 */
	public function edit()
	{
		$categoryLogic = D('Category','Logic');
		if(IS_POST)
		{
			if(!$this->logic->create())	
				$this->error($this->logic->getError());
			$this->logic->save();
			$aid = I('post.aid');
			$cid = I('post.category_cid');
			$verifystate = I('post.verifystate');
			$this->logic->update_ext($aid,$cid);
			$cur = $categoryLogic->get_one($cid);
			if($cur['cat_type']==4)
				$url = U('Index/copyright');
			else
				$url = U('index',array('category_cid'=>$cid,'verifystate'=>$verifystate));
			$this->success('修改成功',$url);	
		}
		else
		{
			$_SESSION['keditor'] = null;
			$cid = I('get.category_cid');
			$this->base($cid);
			// 当前访问的栏目信息
			$cur = $categoryLogic->get_one($cid);
			$this->assign('curCatType',$cur['cat_type']);
			$this->assign('curCname',$cur['cname']);
			$this->assign('curCid',$cur['cid']);

			$pids = D('Category')->get_parent_cid($cid);
			$this->assign('pid',$pids[count($pids)-1]);


			$aid = I('get.aid');	
			$data = $this->logic->get_one($aid,$cid);
			$this->assign('data',$data);
			
			$pics = D('ArticlePic','Service')->get_all($aid);
			$this->assign('isattr',$pics['isattr']);
			$this->assign('pics',$pics['pics']);

			//扩展表表单
			$extForm = D("ModelField","Logic")->get_field_form($cur['model_mid'],$data);
			$this->assign('extForm',$extForm);


			// 读取栏目的文档类型表单
			$attrLogic = D('Attr',"Logic");
			$typeid = $cur['type_typeid'];
			$attrForm = $attrLogic->get_attr_form($typeid,$aid);
			$this->assign('attrForm',$attrForm);


			$this->display();
		}
		
	}
	


	
	/**
	 * [ajax_category 所有栏目]
	 * @return [type] [description]
	 */
	public function ajax_category()
	{
		$category = D('Category','Logic')->article_ztree();
		$this->ajaxReturn($category);
	}


	public function ajax_del_pic()
	{
		if(!IS_AJAX) $this->error('链接错误');

		$ArticlePicLogic = D('ArticlePic','Logic');
		if(!$ArticlePicLogic->del_pic())
			$this->ajaxReturn(array('status'=>0,'info'=>$ArticlePicLogic->getError()));
		$this->ajaxReturn(array('status'=>1,'info'=>'图片删除成功'));
	}

	public function ajax_attr_del_pic()
	{
		if(!IS_AJAX) $this->error('链接错误');

		$ArticlePicLogic = D('ArticlePic','Logic');
		if(!$ArticlePicLogic->del_attr_pic())
			$this->ajaxReturn(array('status'=>0,'info'=>$ArticlePicLogic->getError()));
		$this->ajaxReturn(array('status'=>1,'info'=>'图片删除成功'));
	}



	
}