<?php
/**[栏目控制器]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-14 15:30:10
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:16:19
 */
namespace Admin\Controller;
class CategoryController extends PublicController{
	
	public function _initialize()
	{
		parent::_initialize();

		$this->assign('model',D('Model','Logic')->get_all());
		$this->assign('type',D('Type','Logic')->get_all());
		$this->assign('category',$this->logic->get_all());
		
		if(ACTION_NAME=='add')
		{
			$parent = $this->logic->get_one(I('get.pid'));
			$this->assign('parent',$parent);
		}
		if(ACTION_NAME=='edit')
		{
			$pid = $category[I('get.cid')]['pid'];
			if($pid)
			{
				$parent = $this->logic->get_one();
				$topCname = $parent['cname'];
			}
			else
			{
				$topCname = '顶级栏目';
			}

			$this->assign('topCname',$topCname);
		}
	}

	/**
	 * [index 所有栏目]
	 * @return [type] [description]
	 */
	public function index()
	{
		$this->display();
	}
	
	public function get_data()
	{
		$parent = $this->logic->get_one(I('get.id'));
		$this->ajaxReturn(array('status'=>1,'info'=>$parent));
	}

}