<?php
/**[后台公用控制器]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-14 15:33:25
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-26 11:10:47
 */
namespace Admin\Controller;
use Common\Controller\ExtendController;
use Think\Page;
use Think\Auth;
class PublicController extends ExtendController
{

	public $logic;
	public $map;
	public function _initialize()
	{


		// 实例化模型
		$controllerName = CONTROLLER_NAME;
		$notAuth = in_array($controllerName,explode(',',C('NOT_D_CONTROLLER')));
		if(!$notAuth)
		{

			$this->logic = D($controllerName,'Logic');
		}
		//define('MODULE_ACTION',strtolower(CONTROLLER_NAME.'_'.ACTION_NAME));



		//验证是否登录
		if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name']))
		{
			$this->redirect('Login/index');
		}
			
		// 验证是否锁定
		$user = D("User")->where(array('username'=>$_SESSION['user_name']))->find();

		
		if($user['is_lock'])
		{
			$this->redirect("Login/out");
		}



		// 验证权限
		if(!in_array(session('user_id'), C('auth_superadmin')) && in_array(ACTION_NAME,C('auth_action_name')) )
		{
			// 权限验证
			$auth = new Auth();

			if(!$auth->check(strtolower(MODULE_NAME . '-' . CONTROLLER_NAME . '-' . ACTION_NAME),session('user_id')))
					die('您没有相关权限！');

		}
	}
	
	/**
	 * [index 所有数据]
	 * @return [type] [description]
	 */
	public function index()
	{
		// 设置查询条件
		$this->map = $this->_search();
		$data =  $this->_list();
		$this->display();
	}


	/**
	 * [_search 设置查询条件]
	 * @return [type] [description]
	 */
	public function _search()
	{
		$map = array();
		$fields = $this->logic->getDbFields();
		if($fields)
		{
			foreach($fields as $v)
			{
				
				if(I('get.'.$v,null)!==null&& I('get.'.$v,null)!=0)
				{
					switch ($v)
					 {
					 	// 栏目分类
						case $v=='category_cid':
							$cids = D('Category')->get_child_cid(I('get.'.$v));
							$map[$v] = array('in',$cids);
							break;
						case $v=='goodscate_cid':
							$cids = D('GoodsCate')->get_child_cid(I('get.'.$v));
							$map[$v] = array('in',$cids);
							break;
						case $v=='flag':
							$map[] = "find_in_set('".I('get.'.$v)."',flag)";
							break;						
						default:
							$map[$v] = I('get.'.$v);
							break;
					}
					
				}
			}
		}

		$keyword = I('get.keyword');
		$keytype = I('get.keytype');
		if($keyword && $keytype)
		{

			$map[$keytype] = array('like','%'.$keyword.'%'); 
		}
		if(I('get.search_begin_time'))
		{
			$map[] = strtolower(CONTROLLER_NAME).'.addtime >='. strtotime(I('get.search_begin_time'));
		}
		if(I('get.search_end_time'))
			$map[] = strtolower(CONTROLLER_NAME).'.addtime <='.strtotime(I('get.search_end_time'))+24*60*60;

	
		return $map;

	}


	/**
	 * [_list 列表]
	 * @return [type] [description]
	 */
	public function _list()
	{
		$data = array();
		// 排序字段 默认为表的主键
		$order = I('post._order',$this->logic->getPk());
		// 排序方式 默认为降序排列
		$sort  = I('post._sort','desc');
		// 统计
		if(I('get.role')==1 && CONTROLLER_NAME  == 'User' )
		{
			if(I('get.group_id'))
			{
				$this->map['group_id'] = I('get.group_id');
				D('AdminView')->viewFields['auth_group_access'] = array(
					'group_id',
					'_on'=>'auth_group_access.uid=user.uid'
				);
			}
			$count = D('AdminView')->where($this->map)->count();
		}
		else
			$count = $this->logic->alias(strtolower(CONTROLLER_NAME))->where($this->map)->count();
	
		if($count>0)
		{
			// 每页显示记录数
			$listRows = I('post.numPerPage',C('PAGE_LISTROWS'));
			// 实例化分页类 传入总记录数和每页显示的记录数
			$page = new Page($count,$listRows);
			// 当前页数
			$currentPage = I(C('VAR_PAGE'),1);
			// 进行分页数据查询
			$data = $this->logic->get_all($this->map,$order,$sort,$currentPage,$listRows);
			// 分页显示输出
			$show = $page->show();
			//模板赋值
			$this->assign('page',$show);
	
		}
		$this->assign('data',$data);
	}



	/**
	 * [add 添加]
	 */
	public function add()
	{
		if(IS_POST)
		{
			if(!$this->logic->create())
				$this->error($this->logic->getError());
			$this->logic->add();
			if(CONTROLLER_NAME=='ModelField')
				$this->success('添加成功',U('index',array('mid'=>I('post.mid'))));
			elseif(CONTROLLER_NAME=='User')
				$this->success('添加成功',U('index',array('role'=>I('post.role'))));
			elseif(CONTROLLER_NAME=='Goods')
				$this->success('添加成功',U('index',array('goodscate_cid'=>I('post.goodscate_cid'),'verifystate'=>I('post.verifystate'))));

			elseif(CONTROLLER_NAME=='ShippingArea')
				$this->success('添加成功',U('index',array('shipping_id'=>I('post.shipping_id'))));
			elseif(CONTROLLER_NAME=='Attr')
				$this->success('添加成功',U('index',array('typeid'=>I('post.typeid'))));
			elseif(CONTROLLER_NAME=='Ad')
				$this->success('添加成功',U('index',array('verifystate'=>I('post.verifystate'))));
			else
				$this->success('添加成功',U('index'));
		
		}
		else
		{
			$this->display();
		}
	}

	/**
	 * [edit 编辑模型]
	 * @return [type] [description]
	 */
	public function edit()
	{
		if(IS_POST)
		{
			if(!$this->logic->create())
				$this->error($this->logic->getError());
			$this->logic->save();
			if(CONTROLLER_NAME=='ModelField')
				$this->success('编辑成功',U('index',array('mid'=>I('post.mid'))));
			elseif(CONTROLLER_NAME=='User')
				$this->success('编辑成功',U('index',array('role'=>I('post.role'))));
			elseif(CONTROLLER_NAME=='Goods')
				$this->success('编辑成功',U('index',array('goodscate_cid'=>I('post.goodscate_cid'),'verifystate'=>I('post.verifystate'))));
			elseif(CONTROLLER_NAME=='ShippingArea')
				$this->success('编辑成功',U('index',array('shipping_id'=>I('post.shipping_id'))));
			elseif(CONTROLLER_NAME=='Attr')
				$this->success('编辑成功',U('index',array('typeid'=>I('post.typeid'))));
			elseif(CONTROLLER_NAME=='Ad')
				$this->success('编辑成功',U('index',array('verifystate'=>I('post.verifystate'),'position_pid'=>I('post.position_pid'))));
			else
				$this->success('编辑成功',U('index'));
		}
		else
		{
			$pk = $this->logic->getPk();
			$id  = I('get.'.$pk);
			$data = $this->logic->get_one($id);
			if(!$data)
				$this->error('信息不存在');
			$this->assign('data',$data);
			$this->display();
		}
	}

	/**
	 * [beachdelete 删除模型]
	 * @return [type] [description]
	 */
	public function beachdelete()
	{
		$pk = $this->logic->getPk();
		$id = I('post.'.$pk);
		switch (true) 
		{
			// 排序
			case isset($_POST['update_sort']):
				$sort = I('post.sort');
				$this->logic->update_sort($id,$sort);
				$msg= '排序成功';
				break;
			// 审核
			case isset($_POST['update_check_state']):
				$this->logic->update_check_state($id,2);
				$msg= '审核成功';
				break;
			// 取消审核
			case isset($_POST['update_cancle_state']):
				$this->logic->update_check_state($id,1);
				$msg= '审核取消成功';
				break;
			// 批量删除
			case isset($_POST['update_del']):
				$id = implode(',', $id);
				$status = $this->logic->del($id);
				if(!$status)
					$this->error($this->logic->getError());
				$msg= '删除成功';
				break;
			// 锁定
			case isset($_POST['update_check_lock']):
				$id = implode(',', $id);
				$status = $this->logic->update_lock($id,1);
				if(!$status)
					$this->error($this->logic->getError());
				$msg= '锁定成功';
				break;
			// 解除锁定
			case isset($_POST['update_cancle_lock']):
				$id = implode(',', $id);
				$status = $this->logic->update_lock($id,0);
				if(!$status)
					$this->error($this->logic->getError());
				$msg= '解除锁定成功';
				break;

			case isset($_POST['update_check_operation']):
				
				$status = $this->logic->update_flag($id,1);
				if(!$status)
					$this->error($this->logic->getError());
		
				$msg= '属性添加成功';
				break;
			case isset($_POST['update_cancle_operation']):
		
				$status = $this->logic->update_flag($id,0);
				if(!$status)
					$this->error($this->logic->getError());
				$msg= '属性删除成功';
				break;
			default:
				$this->error('操作选择错误');
				return false;

		}

		if(CONTROLLER_NAME=='ModelField')
			$this->success($msg,U('index',array('mid'=>I('post.mid'))));
		elseif(CONTROLLER_NAME=='User')
			$this->success($msg,U('index',array('role'=>I('post.role'))));
		elseif(CONTROLLER_NAME=='Article')
			$this->success($msg,U('index',array('category_cid'=>I('post.category_cid'),'verifystate'=>I('post.verifystate'))));
		elseif(CONTROLLER_NAME=='Goods')
				$this->success($msg,U('index',array('goodscate_cid'=>I('post.goodscate_cid'),'verifystate'=>I('post.verifystate'))));
		elseif(CONTROLLER_NAME=='Attr')
				$this->success($msg,U('index',array('typeid'=>I('post.typeid'))));
		elseif(CONTROLLER_NAME=='Ad')
				$this->success($msg,U('index',array('verifystate'=>I('get.verifystate'))));	
		else
			$this->success($msg,U('index'));
	}

	/**
	 * [beachdelete 更新缓存]
	 * @return [type] [description]
	 */
	public function update_cache()
	{
		$this->logic->update_cache();
		if(CONTROLLER_NAME=='ModelField')
			$this->success('缓存更新成功',U('index',array('mid'=>I('get.mid'))));
		elseif(CONTROLLER_NAME=='Attr')
				$this->success('缓存更新成功',U('index',array('typeid'=>I('get.typeid'))));
		else
			$this->success('缓存更新成功',U('index'));
	}
	
	/**
	 * [del 删除模型]
	 * @return [type] [description]
	 */
	 public function del()
	 {
	 	$pk = $this->logic->getPk();
		$id  = I('get.'.$pk);
		$status = $this->logic->del($id);
		if(!$status)
			$this->error($this->logic->getError(),U('index'));
		if(CONTROLLER_NAME=='ModelField')
			$this->success('删除成功',U('index',array('mid'=>I('get.mid'))));
		elseif(CONTROLLER_NAME=='Article')
			$this->success('删除成功',U('index',array('category_cid'=>I('get.category_cid'),'verifystate'=>I('get.verifystate'))));
		elseif(CONTROLLER_NAME=='User')
			$this->success('删除成功',U('index',array('role'=>I('get.role'))));
		elseif(CONTROLLER_NAME=='Goods')
			$this->success('删除成功',U('index',array('goodscate_cid'=>I('get.goodscate_cid'),'verifystate'=>I('get.verifystate'))));
		elseif(CONTROLLER_NAME=='OrderLists')
			$this->success('删除成功',U('index',array('is_deposit'=>I('get.is_deposit'))));
		elseif(CONTROLLER_NAME=='Attr')
				$this->success('删除成功',U('index',array('typeid'=>I('get.typeid'))));
		elseif(CONTROLLER_NAME=='Ad')
				$this->success('删除成功',U('index',array('verifystate'=>I('get.verifystate'))));
		else
			$this->success('删除成功',U('index'));
	 }
	 
	 /**
	 * [ajax_del_attachment 删除附件]
	 * @return [type] [description]
	 */
	 public function ajax_del_attachment()
	 {
	 	if(!IS_AJAX) _404('页面错误');
		$pk = $this->logic->getPk();
		$id  = I('get.'.$pk);
		$field = I('get.field');
		$this->logic->del_attachment($id,$field);
		$this->ajaxReturn(array('status'=>1,'info'=>'删除成功'));
	 }
}