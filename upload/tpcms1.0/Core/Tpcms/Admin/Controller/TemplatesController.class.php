<?php
/**[模板风格管理]
 * @Author: 976123967@qq.com
 * @Date:   2015-06-02 16:02:02
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-06-02 18:24:27
 */
namespace Admin\Controller;
use Third\Dir;
use Third\Xml;
class TemplatesController extends PublicController{
	public function index()
	{

		$dirs = Dir::tree('Templates');
		foreach ($dirs as $tpl)
		{
			if(!is_dir($tpl['path'])) continue;
            $xml = $tpl['path'] . '/config.xml';
            if (!is_file($xml)){
                continue;
            }
            if (!$config = Xml::toArray(file_get_contents($xml))){
                continue;
            }
            $tpl['filename'] = $tpl['name'];
            $tpl['name'] = isset($config['name']) ? $config['name'][0] : ''; //模型名
            $tpl['author'] = isset($config['author']) ? $config['author'][0] : ''; //作者
            $tpl['image'] =  isset($config['image'])?__ROOT__.'/Templates/'.$tpl['filename'].'/'.$config['image'][0] :__ROOT__.'/Core/Tpcms/'.MODULE_NAME.'/View/Public/images/preview.jpg'; //预览图
           
            $tpl['email'] = isset($config['email']) ? $config['email'][0] : ''; //邮箱
            $tpl['current'] = C("WEB_STYLE") == $tpl['filename'] ? 1 : 0; //正在使用的模板
            $style[] = $tpl;
        }
        $this->assign('style',$style);
		$this->display();
	}

    public function ajax_set_templates()
    {

        $data['WEB_STYLE'] = I('post.filename');
        file_put_contents('./Data/Config/theme.inc.php', "<?php\n return ".var_export($data,true).";");
        session('curTheme',$data['WEB_STYLE']);
        $this->ajaxReturn(array('status'=>1,'info'=>'风格设置成功'));
    }
}