<?php
/** [广告控制器]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 11:23:58
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 16:14:14
 */
namespace Admin\Controller;
class AdController extends PublicController{

	public function _initialize()
	{
		parent::_initialize();
		$position = D('Position','Logic')->get_all();
		$this->assign('position',$position);
	}
	
}