<?php
/**[***]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-29 13:11:47
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:48:41
 */
namespace Common\Model;
use Think\Model\ViewModel;
class RArticleViewModel extends ViewModel{

	public $viewFields  = array(
		'r_article'=>array(
			'*',
			'_type'=>'INNER',
		),
		'article'=>array(
			'*',
			'_type'=>'INNER',
			'_on'=>'r_article.raid=article.aid',
		),
		'user'=>array(
			'username','uid',
			'_type'=>'INNER',
			'_on' =>'user.uid=article.user_uid',
		),
		'category'=>array(
			'cid','cname','remark',
			'_type'=>'INNER',
			'_on' =>'category.cid=article.category_cid',
		)
	); 
}