<?php
/** [栏目模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-16 23:41:12
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:44:07
 */
namespace Common\Model;
use Think\Model;
use Third\Data;
class CategoryModel extends Model{

	private $cache;
	public function _initialize()
	{
		$this->cache = S('category');
	}

	/**
	 * [get_parent_list 获取分类的所有父级栏目]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_parent_list($cid)
	{
		return Data::parentChannel($this->cache,$cid);
	}

	/**
	 * [get_parent_cid 所有父级的cid]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_parent_cid($cid)
	{
		$data =$this->get_parent_list($cid);
		$result  = array();
		foreach($data as $v)
		{
			$result[] =$v['cid'];
		}
		return $result;
	}
	
	/**
	 * [get_top_child 获取顶级栏目子集 树状]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_top_child($cid)
	{
		$parentCids = $this->get_parent_cid($cid);
		return Data::channelList($this->cache,$parentCids[count($parentCids)-1],'---');
	}

	/**
	 * [get_child_cid 子栏目的所有的cid]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_child_list($cid)
	{
		$result =Data::channelList($this->cache,$cid,'');
		return $result;
	}
	/**
	 * [get_child_cid 子栏目的所有的cid]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_child_cid($cid)
	{
		$data = $this->get_child_list($cid);
	
		$result = array($cid);
		foreach($data as $v)
		{
			$result[]= $v['cid'];
		}
		return $result;
	}



	
	public function get_top($cid)
	{
		$parents = D('Category')->get_parent_list($cid);
		$parents = array_reverse($parents);
		return $parents[0];
	}

	
}